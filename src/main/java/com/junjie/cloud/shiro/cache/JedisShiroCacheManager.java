package com.junjie.cloud.shiro.cache;

import org.apache.shiro.cache.Cache;

import com.junjie.commons.cache.JunjieRedisCacheManager;

/**
 * 这里的name是指自定义relm中的授权/认证的类名加上授权/认证英文名字
 *
 * @author michael
 */
public class JedisShiroCacheManager implements ShiroCacheManager {

	 private JunjieRedisCacheManager junjieRedisCacheManager;

    @Override
    public <K, V> Cache<K, V> getCache(String name) {
        return new JedisShiroCache<K, V>(name,junjieRedisCacheManager);
    }

    @Override
    public void destroy() {
    }

	public JunjieRedisCacheManager getJunjieRedisCacheManager() {
		return junjieRedisCacheManager;
	}

	public void setJunjieRedisCacheManager(
			JunjieRedisCacheManager junjieRedisCacheManager) {
		this.junjieRedisCacheManager = junjieRedisCacheManager;
	}

   
}
