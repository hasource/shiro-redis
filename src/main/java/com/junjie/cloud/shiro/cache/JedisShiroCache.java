package com.junjie.cloud.shiro.cache;

import java.util.Collection;
import java.util.Set;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;

import com.junjie.commons.cache.JunjieRedisCache;
import com.junjie.commons.cache.JunjieRedisCacheManager;

/**
 * redis shiro cache class
 *
 * @author michael
 */
public class JedisShiroCache<K, V> implements Cache<K, V> {

    private static final String REDIS_SHIRO_CACHE = "shiro-cache:";
    private static final int DB_INDEX = 1;

    private JunjieRedisCacheManager junjieRedisCacheManager;

    private String name;
    private JunjieRedisCache cache  = null;

    public JedisShiroCache(String name, JunjieRedisCacheManager junjieRedisCacheManager) {
        this.name = name;
        this.junjieRedisCacheManager = junjieRedisCacheManager;
        cache = junjieRedisCacheManager.getCache(REDIS_SHIRO_CACHE+name);
    }
    /**
     * 自定义relm中的授权/认证的类名加上授权/认证英文名字
     */
    public String getName() {
        if (name == null)
            return "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SuppressWarnings("unchecked")
    @Override
    public V get(K key) throws CacheException {
        return (V) cache.get(key);
    }

    @Override
    public V put(K key, V value) throws CacheException {
        V previos = get(key);
        cache.put(key, value);
        return previos;
    }

    @Override
    public V remove(K key) throws CacheException {
        V previos = get(key);
        cache.evict(key);
        return previos;
    }

    @Override
    public void clear() throws CacheException {
        //TODO
    }

    @Override
    public int size() {
        if (keys() == null)
            return 0;
        return keys().size();
    }

    @Override
    public Set<K> keys() {
        //TODO
        return null;
    }

    @Override
    public Collection<V> values() {
        //TODO
        return null;
    }

    private String buildCacheKey(Object key) {
        return REDIS_SHIRO_CACHE + getName() + ":" + key;
    }

}
