package com.junjie.cloud.shiro.session;

import java.io.Serializable;
import java.util.Collection;

import org.apache.shiro.session.Session;

import com.junjie.cloud.shiro.SerializeUtil;
import com.junjie.commons.cache.JunjieRedisCache;
import com.junjie.commons.cache.JunjieRedisCacheManager;

/**
 * redis save shiro session class
 *
 * @author michael
 */
public class JedisShiroSessionRepository implements ShiroSessionRepository {

    private static final String REDIS_SHIRO_SESSION = "shiro-session:";
    private static final int SESSION_VAL_TIME_SPAN = 18000;
    private JunjieRedisCacheManager junjieRedisCacheManager;
	private JunjieRedisCache cache  = null;
    public void init(){
    	    cache = junjieRedisCacheManager.getCache(REDIS_SHIRO_SESSION);
    }
    @Override
    public void saveSession(Session session) {
        if (session == null || session.getId() == null)
            throw new NullPointerException("session is empty");
        try {
            long sessionTimeOut = session.getTimeout() / 1000;
            Long expireTime = sessionTimeOut + SESSION_VAL_TIME_SPAN + (5 * 60);
            cache.put(session.getId(), session, expireTime);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("save session error");
        }
    }

    @Override
    public void deleteSession(Serializable id) {
        if (id == null) {
            throw new NullPointerException("session id is empty");
        }
        try {
        	cache.evict(id);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("delete session error");
        }
    }

    @Override
    public Session getSession(Serializable id) {
        if (id == null)
            throw new NullPointerException("session id is empty");
        Session session = null;
        try {
        	session = cache.get(id,  Session.class);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("get session error");
        }
        return session;
    }

    @Override
    public Collection<Session> getAllSessions() {
     //   cache.
        System.out.println("get all sessions");
        return null;
    }
	public JunjieRedisCacheManager getJunjieRedisCacheManager() {
		return junjieRedisCacheManager;
	}
	public void setJunjieRedisCacheManager(
			JunjieRedisCacheManager junjieRedisCacheManager) {
		this.junjieRedisCacheManager = junjieRedisCacheManager;
	}
    

}
