package com.junjie.cloud.shiro;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;

class Food {
	Food() {
		System.out.print("1");
	}
}

class Fruit extends Food implements Serializable {
	Fruit() {
		System.out.print("2");
	}
}

public class Banana extends Fruit {
	int size = 42;

	public static void writeObject(Object o) throws Exception {

		File f = new File("d:/usertmp");

		if (f.exists()) {

			f.delete();

		}

		FileOutputStream os = new FileOutputStream(f);

		// ObjectOutputStream 核心类

		ObjectOutputStream oos = new ObjectOutputStream(os);

		oos.writeObject(o);

		oos.close();

		os.close();

	}

	public static Banana readObject() throws Exception {
		File f = new File("d:", "usertmp");
		InputStream is = new FileInputStream(f);

		// ObjectOutputStream 核心类

		ObjectInputStream ois = new ObjectInputStream(is);

		return (Banana) ois.readObject();

	}

	int yellow = 4;

	public static void main(String[] args) throws Exception {
		// Banana b = new Banana();
		// System.out.println("qqq");
		// b.writeObject(b); // assume correct serialization
		// System.out.println("aaa");
		// b = b.readObject();
		// System.out.println("wwwww");
		// System.out.println("restore " + b.size);
		// NumberFormat nf = NumberFormat.getInstance();
		// nf.setMaximumFractionDigits(4);
		// nf.setMinimumFractionDigits(2);
		// String a = nf.format(3.1415926);
		// String c = nf.format(2);
		 String csv = "Sue,5,true,3";
		  Scanner scanner = new Scanner( csv);
		  scanner.useDelimiter(",");
		  int age = scanner.nextInt();
		  System.out.println(age);
		// b = nf.equals(input);
		// b = nf.parseObject(input);
	}
	// more Banana methods go here
}
